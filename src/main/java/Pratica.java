
import utfpr.ct.dainf.pratica.PontoXY;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author 
 */
public class Pratica {

    public static void main(String[] args) {
        PontoXY p1 = new PontoXY(-3,2,0);
        PontoXY p2 = new PontoXY(0,2,0);
        p1.dist(p2);
        
    }
    
}